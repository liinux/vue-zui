var Event={

	on:function(eventName,callback,useCapture){
		
		if(!useCapture) useCapture=false;
		if(document.addEventListener){
			document.addEventListener(eventName,function(ev){
				 callback.call(this,ev);
			},useCapture);
		}else{
			document.attachEvent(eventName,function(ev){
				 callback.call(this,ev);
			},useCapture);
		}
	},
	off:function(eventName,funcObj,useCapture){
		
		if(!useCapture) useCapture=false;
		if(document.removeEventListener){
			document.removeEventListener(eventName,funcObj,useCapture);
		}else{
			document.detachEvent(eventName,funcObj,true);
		}
	},
	getDomNode:function(ev,current){
		//TODO
		if(current){
			return ev.currentTarget;
		}
		return ev.target;
	},
	supportTouchEvent:function(){
		if("ontouchend" in document){
			return true;
		}
		return false;
	}
};

module.exports= Event;

