import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state={
	routers:[]
};

const mutations = {
	QUERY_ROUTERS(state,routes){
	  	 state.routers=routes;
	}
};

export default new Vuex.Store({
    state,
    mutations,
    strict: process.env.NODE_ENV !== 'production'
});